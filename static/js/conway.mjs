import conway_init, { Board, CellState } from './conway/conway.js'

let board = null
let tickrate = 500
let timer = null

export const rows = 200;
export const cols = 200;

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

export function resetBoard() {
    board = new Board(rows, cols)
    for(let i = 0; i < 4000; i++) {
        board.toggle_cell(getRandomInt(cols), getRandomInt(rows))
    }
}

export function toggleCell(x, y) {
    return board.toggle_cell(x, y)
}

function tick(ctx, boundingRect) {
    board.update()
    drawBoard(ctx, boundingRect)
}

export function stopAnimation() {
    if(timer) {
        clearTimeout(timer)
        timer = null
    }
}

export function startAnimation(context, boundingRect) {
    stopAnimation()
    timer = setInterval(tick.bind(null, context, boundingRect), tickrate)
}

export function drawCell(ctx, x, y, boundingRect, cell) {
    switch(cell) {
        case CellState.Dead: 
            ctx.fillStyle = 'black'
            break
        case CellState.Alive:
            ctx.fillStyle = 'white'
    }
    let w = boundingRect.width / cols
    let h = boundingRect.height / rows

    ctx.fillRect(
        Math.floor(x), 
        Math.floor(y), 
        1, 
        1)
}

export function drawBoard(ctx, boundingRect) {
    ctx.fillStyle = 'black';
    ctx.clearRect(0, 0, boundingRect.width, boundingRect.height)
    ctx.fillRect(0, 0, boundingRect.width, boundingRect.height)
    let cells = board.cells();
    
    for(let i = 0; i < rows; i++) {
        for(let j = 0; j < cols; j++) {
            drawCell(ctx, j, i, boundingRect, cells[i * cols + j])
        }
    }
}

export async function init(input) {
    await conway_init(input)
    resetBoard()
}