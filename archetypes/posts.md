---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
lastmod: {{ now.Format "2006-01-02" }}
draft: true
keywords: []
description: ""
tags: []
categories: []
author: "Kaelan Mikowicz"
JSONLDTemplate: post.json
---