---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
lastmod: {{ now.Format "2006-01-02" }}
draft: true
description: ""
author: "Kaelan Mikowicz"
image:
repo:
devpost:
JSONLDTemplate: project.json
---