---
title: Kaelan Mikowicz
subtitle: My personal website
description: Software Engineer. UC Davis June '20 Grad
comments: false
menu: 
    main:
        name: About
        weight: -100
JSONLDTemplate: home.json
---

I am a software developer, hackathon enthusiast, open source guy.

Currently building out this website while under quarantine. Working on a driver for my MSP432P401R in Rust.

# Organizations

*Alumni* - [HackDavis](https://hackdavis.io)

*Alumni* - [UC Davis Formula Racing Team](http://frucd.org/)