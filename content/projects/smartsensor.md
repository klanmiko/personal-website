---
title: SmartSensor
devpost: https://devpost.com/software/smart-sensor
image: original.jpg
summary: HackDavis 2017 award winning project. We attempted to count the number of people in a room by placing two sensors next to each other. We then displayed room occupancy on a central web UI.
publishDate: "2017-02-26"
JSONLDTemplate: project.json
tags:
    - Hackathon
    - HackDavis
    - IoT
    - PHP
    - Awards
    - Web
    - Embedded
---
# Synopsis
We wanted to design a better motion sensor for our dorm bathrooms, as the ones we had were prone to turning off after a short time interval. Unlike the timer based system in the dorms, ours would attempt to count the number of people in a room by placing two sensors next to each other. Based on the order which each sensor was triggered, we could determine whether a person was exiting or entering the room.

We then tried to iterate on this idea. If we had multiple of these devices placed throughout a building, then it could be possible to track occupancy throughout a building. This information could then be presented on a centralized web interface, largely for use in a security context. An example use case is during a fire, firefighters could use this information to identify trapped individuals and better target rescue efforts. A second application is to take this occupancy data as a signal in a larger dataset about a building. For example, we thought it would be interesting to compare occupancy to utilities usage in a building. Simple analysis could model the fixed energy costs and the variable usage rate for rooms in a building.

# Architecture

We knew we needed at least two motion sensors and an IoT board that would allow us to collect and publish data from our sensors to our web interface. We ended up picking the Particle.io boards that were being loaned out at the event. The overall system comprised of a few components:

**Hardware**:
* Particle.io board keeps track of current count
* Two motion sensors, sensitivity tuned with resistors
* LCD display
* Sample lights
* Control loop
    * Check motion sensor state
    * Update local count and Particle Cloud
    * Animate display
    * Toggle lights

**Software**:
* Custom hosted PHP backend
    * Stores Energy usage data from OSISoft API in MySQL Database
    * Web hooks for changing count of people in a room
    * Stores building occupancy in database
* Web interface
    * Polls Particle.io board for current count, displays on floorplan
    * Graphs energy usage data against room occupancy count

# Awards
Best Environmental Hack

1st Place Particle Prize