---
title: HackDavis Applications App
draft: true
repo: https://github.com/HackDavis/applications
publishDate: "2018-11-15"
JSONLDTemplate: project.json
tags:
    - HackDavis
    - Vue.js
    - Python
    - Flask
    - PostgreSQL
    - Nginx
    - ORM
    - Authorization
    - Web
---
I made a thing for applications