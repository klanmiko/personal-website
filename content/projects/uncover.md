---
title: Uncover your usage
publishDate: "2018-04-18"
summary: TreeHacks 2018 project to predict home energy usage based on key factors.
image: uncover.jpg
devpost: https://devpost.com/software/uncover-your-usage
JSONLDTemplate: project.json
tags:
    - Hackathon
    - Vue.js
    - Machine Learning
    - Python
    - Web
---

# Synopsis

The US Energy Information Administration provides a large data set of over 12,000 different houses across the country spanning 900 different factors of a household (e.g number of rooms, size of the garage, average temperature) related to annual energy consumption. The data seemed very useful, but too expansive to use. So we wanted to see if we could make it easier to find which factors influence your energy consumption the most.

We wanted to figure out the key factors from the data set that would determine the annual energy consumption of a house, and then build a model on this data that could be accessed through a web UI. They could input information about their home and then figure out what changes to their home would create the greatest impact on their energy usage.

## Implementation

I performed PCA on the input dataset to figure out the top few elements in the dataset. Thomas ran regression tests between around 1000 variables and energy usage to figure out the most correlated input variables. Once we had the lists of variables from these two methods, we combined them and fed them into a Deep Neural Network made by Sergei.

I built the frontend in Vue.js and sent the form data to our python backend running our trained model. The trained model inferred the energy usage for the parameters and this was displayed for the user.