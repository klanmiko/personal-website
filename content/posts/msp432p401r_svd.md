---
title: Blinky in Rust
publishDate: "2020-03-25"
draft: true
JSONLDTemplate: post.json
---

I have an MSP432P401R launchpad.

There's a lot of hardware lying around my house, from Arduino's to the Particle.io board that I won at HackDavis2017.
Interestingly enough I've never touched that product after winning it. Not because it is bad, but because
hardware projects take a considerable amount of time to work on, and school is hard.
