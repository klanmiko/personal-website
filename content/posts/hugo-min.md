---
title: Hugo Minifier
publishDate: "2020-06-14"
summary: Hugo broke my website to save one character
JSONLDTemplate: post.json
---
# Bug
The minifier turns `flex: none` into `flex: 0 0`. These are slightly different as `none` refers to `flex: 0 0 auto` where `flex-basis: auto`, and `flex: 0 0` is interpreted as `flex-basis: 0`. This causes some issues with my website, namely bulma columns relies on flex: none to allow the width of each column to be set by the width property. If I minify 

[MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/flex-basis) shows that when both the flex-basis and width are set, then the flex-basis takes priority. Thus the minification should not perform this transformation just to save one character.

# Fix
Turns out I am running Hugo v0.69. The new version of Hugo v0.70 has an updated minifier that does not do this, and this is what I publish my website with. In order to view content locally all I needed to do was update Hugo.