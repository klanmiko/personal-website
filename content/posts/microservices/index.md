---
title: A Talk on Microservices
publishDate: "2020-04-08"
images:
- imperialseal.jpg
- kubernetes.png
JSONLDTemplate: post.json
---
During Winter quarter, I asked for the opportunity to present about a topic in my senior design class, any of my choosing. I chose to give a talk about microservices, because 
a) it's a buzzword, b) it's a mainly distributed software architecture, c) I worked with microservices in Golang for my summer internship at DJI Inc. Having given a similar talk
at DJI for the end of my internship, I thought it would make good content to further expand on the slides I had written.

# Slide Deck
[Here's a link to the slide deck](https://docs.google.com/presentation/d/1kg08gqjjem9AfwjVnhLh_MRKX7lcFszuYzk73jvHVts/edit?usp=sharing). I'll probably come back to this article
once I've figured out how I want the google docs shortcode embed to look, along with other feature improvements to this site. Many of the images in this slide deck come from
various Nginx articles on microservices design, as they have written a [nice set of blog posts that help explain the topic in detail](https://www.nginx.com/blog/introduction-to-microservices/) that I have used as a resource when creating these slides.

I should mention a couple more interesting parts of the slide deck.

**Blue pill**: Microservices solve organizational problems

**Red pill**: Microservices cause technical problems

Indeed, I think microservices are sold at some business-level or some technical level on the basis of scaling, both in the human and operating resources. It takes considerable effort
to work with microservices, to get them right, and to not bottleneck them nor introduce bugs trying to maintain data consistency. In some situations, implementing a fully correct,
and effective microservice solution is not to be preferred to more traditional methods. 


{{< rfigure src="imperialseal.jpg" title="Galactic Empire" >}}
{{< rfigure src="kubernetes.png" title="Kubernetes" >}}

There's the Simian Army. There's a great Netflix talk on chaos testing, [There's a particular monkey](https://github.com/Netflix/chaosmonkey) which is known to live in 
production clusters randomly terminating pods. There's a reason why Netflix developers have a really high salary.


# Retrospective
I learned that there is a lot about microservices that I don't know. While I worked at DJI most of my work was aimed towards either 
1. fixing data race bugs 
2. migrating from a mono-repo to a multi-repo codebase on a different CI platform (that took weeks) 
3. adding new GraphQL queries, GraphQL schemas, database schemas to fit our new requirements weekly 
4. creating a new microservice that was to aggregate health checks and related statistics.

Most of our service-to-service calls were synchronous throughout our application, and because we implemented strong consistency, we didn't need many of the more complex eventual consistency
techniques that I talked about in my presentation. So going over the content for my presentation I learned a few things which I wish i could have tried during my internship.

I wish I had more knowledge; I also wish I had more time. I planned out this presentation for just about under an hour, somewhere close to 45 minutes. 
Before I gave the presentation I was notified that I would have 15 minutes to speak. I had a chance to remove slides from the presentation, but I thought I would
be able to skip through them.

I spoke to a room of about 70 people, in the evening no less, and there were a few presentations waiting to go after me. I had the conflicting goal of having the presentation be an 
introductory talk, and a comprehensive guide, summarily comprehensive in the fashion that many college lectures are given. This would prove to not work out in my favor. For the easier
parts of my talk, people seemed interested as they learned what microservices are. Then I hit them with sagas, two phase commits, 
kubernetes deployments, testing methodology, and the works. I now understand what it looks like to be overwhelmed with information; it certainly did not help that I was helplessly over my
time limit for the talk. I should have kept the content focused, and realized that some topics were worth expanding into multiple presentations, but that's a good lesson to learn.