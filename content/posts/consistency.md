---
title: Learning how to count
publishDate: "2020-11-27"
summary: HackDavis mobile apps had an exploit for teams to get infinite points, and how I responded. 
JSONLDTemplate: post.json
---
# The App
I talk about my experience making the 2019 HackDavis mobile app [in this post](https://dev.to/klanmiko/flutter-and-parse-1f71). Attendees could collect badges through the app by entering in codes that were given out at the end of workshops to incentivize participation. The app had a leaderboard that showed the top few teams in terms of badge count, and the top team would win a gift card at the end of the Hackathon. Thus, it was important to make sure that the vote count was fair and accurate.

# The Bug
After the first workshop had finished, I was alerted by an attendee that a team on the leaderboard had accrued 36 points. I was immediately confused as we only allowed 4 members in a team and only one workshop code had been given out. We also validated the badge codes so that only valid codes could be entered in.

I logged into our Parse Server backend and looked at our data. We stored the badge codes each user had collected in an array. It was here that I saw the problem: the same code appeared multiple times in a single user's account. Turns out that I forgot to check if the current user already claimed a badge code in the data model.

# The Fix
I talked to the iOS app developer to see if he had made the same bug, and he did.

Instead of checking for consistency at update, the solution here was to make sure the badge codes were unique when they were aggregated for the leaderboard. The end.