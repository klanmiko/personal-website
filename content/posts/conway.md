---
title: Conway's Game of Life
publishDate: "2020-04-17"
JSONLDTemplate: post.json
---
# John Conway
A few days ago I woke up to news that John Conway, British mathematician and inventor of the "Game of Life" cellular automata, had passed away due to
Coronavirus. At UC Davis, Conway's game of life is often the final programming assignment of our intro to programming course, and I had implemented
something similar when I took the intro course.

# Demo
I wanted to put up a running Game of Life simulation on this blog, and I chose to compile from another language into webassembly for the game. A while ago, I was involved in a research project to try to compile Alpine Linux to the browser. That was not completely successful, but I learned a lot about
Emscripten and asm.js/Webassembly through my research and I thought I would leverage those skills here.

After doing a bit of research on the current state of WebAssembly, I found out that the Rust community is building a toolchain for compiling Rust into WebAssembly.
There's also wasi, which is a platform level specification of libraries available to a WebAssembly application. Since I'm writing a driver for a TI board in Rust,
I thought I might explore Rust further by building Conway's game of life.

As a side note, I could have built it in C++ or Golang, and it might have taken me less time, but I thought this was more interesting. Also, while learning about WebAssembly in 
general, I noticed that there are projects on the web like [vue](https://github.com/norunners/vue) that is trying to build a frontend framework in Golang, also [Seed](https://seed-rs.org/). The name sounds familiar...
I think as a frontend developer it's impossible to both keep up with advances in the tools you use, while watching out for new tools that you may need to learn. For example, since
web components has gained adoption of (its specs)[https://www.webcomponents.org/specs] in major browsers, frontend frameworks now seem outdated and bloated. Svelte is becoming popular,
and I personally do want to try using a web components framework for my next project. Anyways, if you thought VueJS would have a problem with the vue golang framework, you would be wrong.
The [Columbia Insurance Company would have a problem with both](http://tmsearch.uspto.gov/bin/showfield?f=doc&state=4810:8eamdl.2.1).

{{<conway>}}

Souce for this demo is available at https://github.com/klanmiko/wasm-game-of-life